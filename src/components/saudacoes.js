import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';

export default function Saudacoes() {
    return (
        <View style={styles.container}>
            <Text>Olá Mundo!</Text>
            <Text>Hello World!</Text>
            <Text>Hallo Welt!</Text>
            <Text>Bonjour le monde!</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    menu: {
        padding: 10,
        margin: 5,
        backgroundColor: "lightblue",
        borderRadius: 5,
    }
});
