import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button, TouchableOpacity, ScrollView } from 'react-native';
import { useEffect, useState } from 'react'

export default function ContagemCliques() {
    const [count, setCount] = useState(0);

    return (

        <View style={styles.container}>
                <Text>Clique para contar: {count}</Text>
                <TouchableOpacity
                    style={styles.teste}
                    onPress={() => setCount(count + 1)}
                >
                    <Text>Clique!</Text>
                </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    teste: {
        padding: 10,
        margin: 5,
        backgroundColor: "lightblue",
        borderRadius: 5,
    }
});
