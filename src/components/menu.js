import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';

export default function Menu({ navigation }) {
    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.menu}
                onPress={() => navigation.navigate('ContagemCaracteres')}
            >
                <Text>Contagem de Caracteres</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.menu}
                onPress={() => navigation.navigate('ContagemCliques')}
            >
                <Text>Contagem de Cliques</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.menu}
                onPress={() => navigation.navigate('ListaTextos')}
            >
                <Text>Lista de Textos</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.menu}
                onPress={() => navigation.navigate('Saudacoes')}
            >
                <Text>Saudações</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.menu}
                onPress={() => navigation.navigate('Inatividade')}
            >
                <Text>Inatividade</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    menu: {
        padding: 10,
        margin: 5,
        backgroundColor: "lightblue",
        borderRadius: 5,
    }
});
