import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Menu from './src/components/menu';
import ContagemCaracteres from './src/components/contagemCaracteres';
import ContagemCliques from './src/components/contagemCliques';
import ListaTextos from './src/components/listaTextos';
import Saudacoes from './src/components/saudacoes';
import Inatividade from './src/components/inatividade';

const Stack = createStackNavigator();

export default function App() {
  return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Menu" component={Menu} />
          <Stack.Screen name="ContagemCaracteres" component={ContagemCaracteres} />
          <Stack.Screen name="ContagemCliques" component={ContagemCliques} />
          <Stack.Screen name="ListaTextos" component={ListaTextos} />
          <Stack.Screen name="Saudacoes" component={Saudacoes} />
          <Stack.Screen name="Inatividade" component={Inatividade} />
        </Stack.Navigator>
      </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
